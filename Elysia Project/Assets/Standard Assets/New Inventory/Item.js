/*
this script is made to show you one way picking up items could possibly be done.
you can find others ways to do this but using the same techniques i do to add item :P.
*/

//Original Script by Eddy//
//-----------------------//
//      Item MkII        //
//Edited by Stewart Grant//
//        and            //
//   Joseph Boddy        //
//-----------------------//

var inventoryIcon:Texture2D;//this is what will be displayed in the demos On Screen GUI.
var isStackable=false;//If this is true it will be stacked in the inv. if not then it wont!
//Stacking will be added in the next update.
var maxStack=9;//each stack can be this big.
var Message:String;






function HIT(){//When you click an item
	 playersinv=FindObjectOfType(Inventory);//finding the players inv. I suggest when making
	//a game, you make a function that picks up an item within the player script. and then have the inventory
	//referneced from its own variable. OR since the playerscript would be attached to the inv i suggest you
	//do GetComponent(Inventory).AddItem, This way multiple players can have inventorys.
	playersinv.AddItem(this.transform);
	MoveMeToThePlayer(playersinv.transform);//moves the object, to the player
	
	}






function MoveMeToThePlayer(theplayer:Transform){//This will basically make the item stay where the player is
//as long as its in the players inventory. This can also be done multiple ways, but ill stick with an easy one.
	transform.collider.isTrigger=true;//makes it undence.
	transform.renderer.enabled=false;//makes it invisible
	transform.parent=theplayer;//makes the object parented to the player.
	transform.localPosition=Vector3.zero;//now that the item is parented to the player
	//i can set the localPosition to 0,0,0 and it will be ontop of the player. and move around with him/her
}


/*
Alternate ways to make objects stay with a player. is just move it to an
unused part of the map and deactivate it like i did. Its invisible so it will be unseen
and its untouchable :P.
If you are switching scenes. for world areas or somthing. you can keep inventory objects
alive by doing "DontDestroyOnLoad(this)". These are just some tips. Design however youd
like to :P.
*/