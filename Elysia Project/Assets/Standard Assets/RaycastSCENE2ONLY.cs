﻿using UnityEngine;
using System.Collections;

//------------------------//
//By Joseph Boddy
//Version 1
//31/08/2013
//Interaction Script
//-----------------------//


//Change log//
// Reduced the distance required to interact with an object.

public class RaycastSCENE2ONLY : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Set ray firing location, i.e. Sets the ray to fire to the center of the screen
		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hit;
		//Only activates when E is pressed
        if (Input.GetKeyUp (KeyCode.E)){
			//Fires the ray and checks if it collided with something.
			if (Physics.Raycast(ray, out hit, 100f)){
				//When the ray collides with an object, it will initate the "HIT" subroutine on the script.
				hit.collider.gameObject.SendMessage("HIT", SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}		