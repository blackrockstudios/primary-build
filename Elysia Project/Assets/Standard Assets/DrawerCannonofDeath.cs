using UnityEngine;
using System.Collections;

//----------------------//
//By Joseph Boddy
//Version 1//
//Made 31/08/13
//---------------------//

public class DrawerCannonofDeath : MonoBehaviour
{
	private int test = 0;
	bool IsDrawOpen = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (IsDrawOpen == true && test <= 15)
		{
			test++;
		}
		else
		{
			test = 0;
		}
	}
	void HIT () {
		
		if (IsDrawOpen==false)
		{
			rigidbody.AddForce(Vector3.forward * 4000);
			IsDrawOpen = true;
		}
		if (test >= 10 && IsDrawOpen == true)
		{
			rigidbody.AddForce(Vector3.back * 4000);
			IsDrawOpen = false;
		}
	}
}