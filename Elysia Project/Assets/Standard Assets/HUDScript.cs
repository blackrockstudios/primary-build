﻿//syclamoth wrote this script out on how to show the crosshair.
using UnityEngine;
using System.Collections;

public class HUDScript : MonoBehaviour {
	
	//Connection to Player Internal Scripts
	public GameObject Player;
	
	//Connection to Global Transmitter
	//public GameObject Global;
	
	//Item Status
	public bool object01 = false;
	public bool object02 = false;
	
	
	
	
	public Texture2D crosshairImage;

	public bool show_inventory = false;
	//public Transform target;
	// Use this for initialization
	void Start () {
	show_inventory=false;
		//target=GetComponent("");
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.Tab))
		{	
			Player.SendMessage ("InventoryCheck");
			if (show_inventory == false){
			Screen.lockCursor = true;
			show_inventory = true;	
			}
			else
			{
			Screen.lockCursor = true;
			//Player.SendMessage ("Disable");
			show_inventory=false;	
			}
		}	
	}

    void OnGUI()
    {
		//Crosshair Menu coordinates
    	float xMin = (Screen.width / 2) - (crosshairImage.width / 2);
    	float yMin = (Screen.height / 2) - (crosshairImage.height / 2);

    	GUI.DrawTexture(new Rect(xMin, yMin, crosshairImage.width, crosshairImage.height), crosshairImage);
    	//If inventory is being shown
		if (show_inventory==true){
			//Player.SendMessage ("Disable"); 

		}
	}


}

